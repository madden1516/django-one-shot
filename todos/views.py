from django.shortcuts import render, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm


# Create your views here.
def todo_list_list(request):
    todo_list = TodoList.objects.all()
    context = {"todo_list_list": todo_list}
    return render(request, "todos/list.html", context)


def todo_list_detail(request, id):
    todo_detail = TodoList.objects.get(id=id)
    context = {"todo_list_detail": todo_detail}
    return render(request, "todos/detail.html", context)


def todo_list_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            return redirect("todo_list_list")
    else:
        form = TodoListForm()
    context = {"form": form}
    return render(request, "todos/detail.html", context)
